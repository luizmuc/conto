function coletaAutomatica() {
    if (grainR >= 85 && grainPerk == true) {
        alert("Você já tem essa habilidade!");
    }
    if (grainR >= 85 && grainPerk == false) {
        grainPerk = true;
        grainR -= 85;
        resourceCheck();
        setInterval('coletarAutomaticamente()', 3500);
    }
    if (grainR < 85) {
        alert("Você não tem grãos suficientes");
    }
}

function superSaude() {
    if (grainR <= 149) {
        alert("Você não tem grãos suficientes");
    }
    if (grainR >= 150 && selfHeal == true) {
        alert("Você já tem essa habilidade!");
    }
    if (grainR >= 150 && selfHeal == false) {
        selfHeal = true;
        grainR -= 150;
        resourceCheck();
        setInterval('selfHealer()', 1000);
    }
}

function superForca() {
    if (grainR <= 250) {
        alert("Você não tem grãos suficientes");
    }
    if (grainR >= 250 && extraDamage == true) {
        alert("Você já tem essa habilidade!");
    }
    if (grainR >= 250 && extraDamage == false) {
        extraDamage = true;
        grainR -= 250;
        damage += 50;
        charPageUpdate();
        resourceCheck();
    }
}

function coletarAutomaticamente() {
    grainR++;
    resourceCheck();
}
function selfHealer() {
    if (health < 100) {
        health += 2;
        charPageUpdate();
    }
    if (stamina < 100) {
        stamina += 5;
        charPageUpdate();
    }
}

/* Button 1 */
$(document).on("click", "button.cooldown1", function () {
    if (curLoc == locations[0]) {
        b1Clicks++;
        grainR++;
        curLoc = locations[0];
        curDesc = locDesc[0];
        curActions = locActions[0];
        var btn = $(this);
        btn.prop('disabled', true);
        setTimeout(function () {
            btn.prop('disabled', false);
            if (!document.body.contains(document.getElementById('grainR'))) {
                document.getElementById('resources').innerHTML += "<li id='grainR' class='list-group-item'>Grãos: <span id='grainID'>" + grainR + "</span></li>";
            } else {
                document.getElementById('grainR').innerHTML = "Grãos: <span id='grainID'>" + grainR + "</span>";
            }
            switch (true) {
                case (b1Clicks <= 1):
                    chatMessage('Todos os dias você trabalhava nos campos. Este é o significado da vida. Pelo menos é o que o você achava. Quanto tempo se passou desde a grande chuva? Você não consegue se lembrar. Você não pode lembrar. Aqueles que lembram ou questionam demais desaparecem e nunca mais são vistos.');
                    break;
                case (b1Clicks == 3):
                    chatMessage('Suas costas doem, mas você continua a colher os grãos. Qualquer coisa para sobreviver. Grãos são a nova moeda agora');
                    break;
                case (b1Clicks == 5):
                    chatMessage('"Trabalho é salvação", eles continuam dizendo. Você silenciosamente questiona a verdade por trás dessas palavras que vem do Abrigo Nuclear.');
                    break;
                case (b1Clicks == 10):
                    chatMessage('Você pode se lembrar vagamente da vida antes da chuva. Pensar nisso seria uma blasfêmia. Você rapidamente afasta os seus pensamentos.');
                    break;
                case (b1Clicks == 15):
                    chatMessage('Um guardião do Abrigo Nuclear se aproxima. Eles são as pessoas que protegem os trabalhadores exterminando pragas que perturbam o equilíbrio. Ele lhe entrega uma pequena picareta e diz para você ir à mina de ferro abandonada e trazer alguns minerios. ([Mapa] -> Mina de Ferro Abandonada-> [Ir])');
                    canMine = 1;
                    $('<button class="cooldown2">Minerar Ferro</button>').hide().appendTo("#buttonsCol").fadeIn(1000);
                    break;
                case (b1Clicks == 23):
                    chatMessage('"Quando será que vira a proxima chuva. Esse tormento não tem fim!');
                    break;
                case (b1Clicks == 30):
                    chatMessage('Incapaz de se manter você pensa que seria bom procurar por alimentos. Pescar no lago seria uma boa ideia.');
                    canFish = 1;
                    $('<button class="cooldown3">Pescar</button>').hide().appendTo("#buttonsCol").fadeIn(1000);
                    break;
            }
        }, 3000);
    } else {
        chatMessage('Você não pode colher grãos sem estar nos campo primeiro!');
    }
});
/* Button 2 */
$(document).on("click", "button.cooldown2", function () {
    if (curLoc == locations[1]) {
        b2Clicks++;
        ironR++;
        curLoc = locations[1];
        curDesc = locDesc[1];
        curActions = locActions[1];
        var btn = $(this);
        btn.prop('disabled', true);
        setTimeout(function () {
            btn.prop('disabled', false);
            if (!document.body.contains(document.getElementById('ironR'))) {
                document.getElementById('resources').innerHTML += "<li id='ironR' class='list-group-item'>Minerios de Ferro: <span id='ironID'>" + ironR + "</span></li>";
            } else {
                document.getElementById('ironR').innerHTML = "Minerios de Ferro: <span id='ironID'>" + ironR + "</span>";
            }
            switch (true) {
                case (b2Clicks <= 1):
                    chatMessage('Minerar é muito mais exaustivo do que colher grãos. Mas preciso disso pra sobreviver. Espero que 5 minérios sejam o suficiente para os Guardiões.');
                    break;
                case (b2Clicks == 3):
                    chatMessage('Você escuta barulhos que não consegue identificar. Podem ser aqueles malditos ratos. Quem sabe? Você só sabe que o barulho te incomoda muito.');
                    break;
            }
            if (ironR == 5) {
                chatMessage('Você ouve um rosnado atrás de você. Você se prepara para o combate. É hora de lutar ou morrer.');
                $(':button').prop('disabled', true);
                setTimeout(function () {
                    $(':button').prop('disabled', false);
                    $('#battle').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#battle').modal('toggle');
                    initializeCombat();
                }, 6000);
            }
        }, 6000);
    } else {
        chatMessage('Você não pode minerar sem estar na Mina Abandonada');
    }
});
/* Button 3 */
$(document).on("click", "button.cooldown3", function () {
    debugger;
    if (curLoc == locations[2]) {
        b3Clicks++;
        fishR++;
        curLoc = locations[2];
        curDesc = locDesc[2];
        curActions = locActions[1];
        var btn = $(this);
        btn.prop('disabled', true);
        setTimeout(function () {
            btn.prop('disabled', false);
            if (!document.body.contains(document.getElementById('fishR'))) {
                document.getElementById('resources').innerHTML += "<li id='fishR' class='list-group-item'>Peixe: <span id='fishID'>" + fishR + "</span></li>";
            } else {
                document.getElementById('fishR').innerHTML = "Peixes: <span id='ironID'>" + fishR + "</span>";
            }
            switch (true) {
                case (b3Clicks <= 1):
                    chatMessage('Pescar parecia mais facil. Você pensa que precisa de pelo menos 4 peixes para se sentir satisfeito');
                    break;
                case (b3Clicks == 2):
                    chatMessage('Um som de conversa enche o ar. Da onde sera que vem?');
                    break;
                case (b3Clicks == 3):
                    chatMessage('As vozes etão vindo do lago? O que diabos é isso?');
                    break;
            }
            if (fishR == 4) {
                cutscene2();
            }
        }, 15000);
    } else {
        chatMessage('Você não pode pescar se não estiver no lago!');
    }
});
/* Battle button */
$(document).on("click", "#battleB", function () {
    $('#battle').modal({ backdrop: 'static', keyboard: false });
    $('#battle').modal('toggle');
    initializeCombat();
});

$(document).on("click", "#comprarEspada", function () {
    if (yourWeapon == weapons[2]) {
        alert('Você já tem o facão equipado')
    }
    if (grainR >= 150) {
        yourWeapon = weapons[2];
        grainR -= 150;
        resourceCheck();
        charPageUpdate();
        alert("Você equipou o Facão")
    } else {
        alert("Você não tem grãos suficientes!");
    }

});

$(document).on("click", "#potCura", function () {
    health = 100;
    alert('Você se curou totalmente');
    charPageUpdate();
});

$(document).on("click", "#startGameButton", function () {
    $('#startScreen').fadeTo(500, 0, function () {
        document.getElementById('startScreen').style.display = "none";
        $('#main').fadeTo(1000, 1, function () {
            document.getElementById('main').style.display = "";
        });
    });
});

$(document).on("click", "#combatButton1", function () {
    if (health > 1) {
        $('#noStaminaAlert').hide();
        var newDamage = Math.floor(Math.random() * ((damage + Math.floor(damage / 2)) - (damage - Math.floor(damage / 2)))) + (damage - Math.floor(damage / 2));
        enemyHealth -= newDamage;
        selfStatsUpdate();
        enemyStatsUpdate();
        $('#enemyIContainer').effect('shake');
        $($.parseHTML('<li>- Você causou ' + newDamage + ' de dano em ' + document.getElementById('enemyName').innerHTML + ' -</li>')).hide().prependTo("#combatLog").fadeIn(1000);
        tCLogs++;
        if (tCLogs >= 5) {
            $('#combatLog li:last').remove();
            tCLogs--;
        }
        if (enemyHealth < 1) {
            if (curLoc == locations[0]) {
                countRat++;
            } else if (curLoc == locations[1]) {
                if (countSaqueadorPest == 0) {
                    countSaqueadorPest++;
                } else {
                    countSaqueador++;
                }
            } else if (curLoc == locations[2]) {
                countSaqueadorPest++;
            } else if (curLoc == locations[3]) {
                countMini++;
            }
            clearInterval(doBattle);
            clearBattle();
            giveEXP();
            dropGrain = Math.floor((Math.random() * 5));
            grainR += dropGrain;
            resourceCheck();
            document.getElementById('combatResults').innerHTML = 'Você ganhou ' + dropGrain + ' grãos ' + '<button class="blankButton" id="exitCombat">[Sair]</button>';
            checkCheck2();
            checkCheck4();
        }
        var btn = $(this);
        btn.prop('disabled', true);
        setTimeout(function () {
            btn.prop('disabled', false);
        }, 3000);
    } else {
        alert('Parece que você morreu!');
    }
});
$(document).on("click", "#combatButton2", function () {
    if (health > 1 && stamina >= 25) {
        $('#noStaminaAlert').hide();
        var newDamage = Math.floor(Math.random() * ((damage + Math.floor(damage / 2)) - (damage - Math.floor(damage / 2)))) + (damage - Math.floor(damage / 2));
        enemyHealth -= Math.floor(newDamage * 2);
        stamina -= 25;
        selfStatsUpdate();
        enemyStatsUpdate();
        $('#enemyIContainer').effect('shake');
        $($.parseHTML('<li>- Você causou ' + Math.floor(newDamage * 2) + ' de dano em ' + document.getElementById('enemyName').innerHTML + ' com seu poderoso ataque -</li>')).hide().prependTo("#combatLog").fadeIn(1000);
        tCLogs++;
        if (tCLogs > 5) {
            $('#combatLog li:last').remove();
            tCLogs--;
        }
        if (enemyHealth < 1) {
            if (curLoc == locations[0]) {
                countRat++;
            } else if (curLoc == locations[1]) {
                if (countSaqueadorPest == 0) {
                    debugger;
                    countSaqueadorPest++;
                } else {
                    countSaqueador++;
                }
            } else if (curLoc == locations[2]) {
                countSaqueadorPest++;
            } else if (curLoc == locations[3]) {
                countMini++;
            }
            clearInterval(doBattle);
            clearBattle();
            giveEXP();
            dropGrain = Math.floor((Math.random() * 5));;
            grainR += dropGrain;
            resourceCheck();
            document.getElementById('combatResults').innerHTML = 'Você ganhou ' + dropGrain + ' grãos ' + '<button class="blankButton" id="exitCombat">[Sair]</button>';
            checkCheck2();
            checkCheck4();
        }
        var btn = $(this);
        btn.prop('disabled', true);
        setTimeout(function () {
            btn.prop('disabled', false);
        }, 8000);
    } else {
        $('#noStaminaAlert').show();
    }
});

$(document).on("click", "#exitCombat", function () {
    $('#battle').modal('hide');
    tCLogs = 0;
    document.getElementById('combatResults').innerHTML = '';
    document.getElementById('combatLog').innerHTML = '';
    charPageUpdate();
});

$(document).on("click", "#restB", function () {
    $('#restPage').modal({ backdrop: 'static', keyboard: false });
    $('#restPage').modal('toggle');
    document.getElementById('restResults').innerHTML = 'Dormindo...<br /> <div class="progress"><div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="30" id="restBar"></div></div>';
    var percent = 1;
    var incPercent = setInterval(function () {
        document.getElementById("restBar").style.width = (percent * 4) + '%';
        percent++;
        if (percent > 25) {
            health = maxHealth;
            stamina = maxStamina;
            charPageUpdate();
            document.getElementById('restResults').innerHTML = 'Descansado! <button class="blankButton" id="exitRest">[sair]</button>';
            chatMessage('Você se sente restaurado. Nada como uma soneca para se sentir novo.');
            clearInterval(incPercent);
        }
    }, 1000);
});

$(document).on("click", "#exitRest", function () {
    $('#restPage').modal('hide');
});

$(document).on("click", "#searchB", function () {
    chatMessage('Você está procurando nos campos...');
    $(':button').prop('disabled', true);
    setTimeout(function () {
        $(':button').prop('disabled', false);
        if (curLoc == locations[0]) {
            var rng = Math.floor(Math.random() * 10) + 1;
            if (rng < 10 && findSearch == true) {
                chatMessage('Você não vê nada de interessante em particular. Apenas campos ao redor. Certos itens têm pouca chance de serem encontrados.');
            } else {
                damage += 20;
                findSearch = false;
                yourWeapon = weapons[1];
                charPageUpdate();
                chatMessage('Você encontra algo entre as folhagens. Parece um taco com pregos. Isso podera ser usado como uma arma');
            }
        }
    }, 3000);
});