// On page load
$(function () {
	locationCheck();
	document.getElementById('stats').innerHTML = '<li class="list-group-item">Nivel: ' + level + ' (' + experience + '/' + reqExp[level] + ')</li><li class="list-group-item">Vida: ' + health + '/' + maxHealth + '</li><li class="list-group-item">Vigor: ' + stamina + '/' + maxStamina + '</li><li class="list-group-item">Dano: ' + damage + '</li>';
	document.getElementById('equipments').innerHTML = '<li class="list-group-item">Arma: ' + yourWeapon + '</li><li class="list-group-item">Armadura: ' + yourArmor + '</li>';
	if (curLoc == locations[0]) {
		chatMessage('Onde quer que você olhe, as zonas alagadas tomam sua visão. Você encontra algumas areas com grãos. Você pode usar esses grãos como moeda de troca.');
	} else if (curLoc == locations[1]) {
		chatMessage('Cheiro de mofo e morte no ar.');
	} else if (curLoc == locations[2]) {
		chatMessage('A luz do sol sobre o lago é lindo. Quanto tempo será que isso vai durar?');
	}
	resourceCheck();
	if (canMine == 1) {
		$('<button class="cooldown2">Minerar Ferro</button>').hide().appendTo("#buttonsCol").fadeIn(1000);
	}
	if (canFish == 1) {
		$('<button class="cooldown3">Pescar</button>').hide().appendTo("#buttonsCol").fadeIn(1000);
	}
});

function initializeCombat() {
	getSelfInfo();
	selfStatsUpdate();
	if (curLoc == locations[0]) {
		if (countRat <= 8) {
			setMonsterInfo("Rato Pestilento", 50, 100, 5);
			getEnemyInfo();
		}
		else {
			setMonsterInfo("Rato Pestilento Chefe", 75, 150, 7);
			getEnemyInfo();
		}
	} else if (ironR == 5 && curLoc == locations[1] && countSaqueadorPest == 0) {
		setMonsterInfo("Saqueador Pestilento", 300, 150, 70);
		getEnemyInfo();
	} else if (curLoc == locations[1]) {
		if (countSaqueador <= 8) {
			setMonsterInfo("Saqueador", 300, 150, 10);
			getEnemyInfo();
		}
		else {
			setMonsterInfo("Soldado Guardião", 450, 150, 80);
			getEnemyInfo();
		}
	} else if (curLoc == locations[2]) {
		if (countSaqueadorPest <= 8) {
			setMonsterInfo("Guardião", 450, 175, 2);
			getEnemyInfo();
		} else {
			setMonsterInfo("Guardião Chefe", 675, 200, 8);
			getEnemyInfo();
		}
	} else if (curLoc == locations[3]) {
		if (countMini <= 0) {
			setMonsterInfo("Rato Pestilento Chefe", 75, 150, 7);
			getEnemyInfo();
		} if (countMini == 1) {
			setMonsterInfo("Soldado Guardião", 450, 150, 80);
			getEnemyInfo();
		} if (countMini == 2) {
			setMonsterInfo("Guardião Chefe", 675, 200, 8);
			getEnemyInfo();
		} if (countMini == 3) {
			setMonsterInfo("Guardião Chefe Superior", 1500, 40, 10);
			getEnemyInfo();
		}
	}
	doBattle = setInterval(function () {
		var rng = Math.floor(Math.random() * 3) + 1;
		if (rng > 2 & enemyStamina >= 25) {
			var newEnemyDamage = (Math.floor(Math.random() * ((enemyDamage + Math.floor(enemyDamage / 2)) - (enemyDamage - Math.floor(enemyDamage / 2)))) + (enemyDamage - Math.floor(enemyDamage / 2))) * 2;
			health -= newEnemyDamage;
			enemyStamina -= 25;
			selfStatsUpdate();
			enemyStatsUpdate();
			$('#playerIContainer').effect('shake');
			$($.parseHTML('<li>- ' + document.getElementById('enemyName').innerHTML + ' causou ' + newEnemyDamage + ' de dano em você com um poderoso ataque -</li>')).hide().prependTo("#combatLog").fadeIn(1000);
			tCLogs++;
			if (tCLogs > 5) {
				$('#combatLog li:last').remove();
				tCLogs--;
			}
		} else {
			var newEnemyDamage = Math.floor(Math.random() * ((enemyDamage + Math.floor(enemyDamage / 2)) - (enemyDamage - Math.floor(enemyDamage / 2)))) + (enemyDamage - Math.floor(enemyDamage / 2));
			health -= newEnemyDamage;
			selfStatsUpdate();
			enemyStatsUpdate();
			$('#playerIContainer').effect('shake');
			$($.parseHTML('<li>- ' + document.getElementById('enemyName').innerHTML + ' causou ' + newEnemyDamage + ' de dano em você -</li>')).hide().prependTo("#combatLog").fadeIn(1000);
			tCLogs++;
			if (tCLogs > 5) {
				$('#combatLog li:last').remove();
				tCLogs--;
			}
		}
		if (health < 1) {
			clearInterval(doBattle);
			clearBattle();
			resourceCheck();
			document.getElementById('combatResults').innerHTML = 'Você morreu. <button class="blankButton" id="exitCombat">[exit]</button>';
			document.getElementById('grainR').style.display = "none";
			document.getElementById('ironR').style.display = "none";
			document.getElementById('fishR').style.display = "none";
			ironR = 0;
			grainR = 0;
			fishR = 0;
			b1Clicks = 0;
			b2Clicks = 0;
			b2Clicks = 0;
			if (extraDamage == false) {
				damage = 10;
			}
			yourWeapon = weapons[0];
			yourArmor = armors[0];
			curLoc = locations[0];
			curDesc = locDesc[0];
			curActions = locActions[0];
			locationCheck();
			health = 100;
			stamina = 100;
			experience = 0;
			chatMessage('Você suspira aliviado com o fim da batalha. Até quando será que você aguenta?');
		}
		resourceCheck();
	}, 3000);
}

function getSelfInfo() {
	$("#playerImageContainer").show();
	document.getElementById('yourName').innerHTML = 'Você';
	document.getElementById('playerImage').innerHTML = '<img src="img/player.png" class="img-fluid rounded mx-auto d-block" style="max-height:300px !important;" />';
	document.getElementById('playerCombatHP').innerHTML = 'Vida: <div class="progress"><div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="' + health + '" aria-valuemin="0" aria-valuemax="' + maxHealth + '" style="width: ' + (health / maxHealth) * 100 + '%" id="playerHP"></div></div><br />';
	document.getElementById('playerCombatSP').innerHTML = 'Vigor: <div class="progress"><div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="' + stamina + '" aria-valuemin="0" aria-valuemax="' + maxStamina + '" style="width: ' + (stamina / maxStamina) * 100 + '%" id="playerSP"></div></div><br />';
}

function getEnemyInfo() {
	$("#enemyImageContainer").show();
	document.getElementById('enemyName').innerHTML = enemyName;
	document.getElementById('enemyImage').innerHTML = '<img src="img/' + enemyName + '.png" class="img-fluid rounded mx-auto d-block" style="max-height:300px !important;" />';
	document.getElementById('enemyCombatHP').innerHTML = 'Vida: <div class="progress"><div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="' + enemyHealth + '" aria-valuemin="0" aria-valuemax="' + enemyMaxHealth + '" style="width: ' + (enemyHealth / enemyMaxHealth) * 100 + '%" id="enemyHP"></div></div><br />';
	document.getElementById('enemyCombatSP').innerHTML = 'Vigor: <div class="progress"><div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="' + enemyStamina + '" aria-valuemin="0" aria-valuemax="' + enemyMaxStamina + '" style="width: ' + (enemyStamina / enemyMaxStamina) * 100 + '%" id="enemySP"></div></div><br />';
	document.getElementById('combatButtons').innerHTML = '<button id="combatButton1" align="center">Ataque</button>&nbsp; <button id="combatButton2" align="center">Ataque Poderoso</button>';
}

function enemyStatsUpdate() {
	document.getElementById("enemyHP").style.width = (enemyHealth / enemyMaxHealth) * 100 + '%';
	document.getElementById("enemySP").style.width = (enemyStamina / enemyMaxStamina) * 100 + '%';
}

function selfStatsUpdate() {
	document.getElementById("playerHP").style.width = (health / maxHealth) * 100 + '%';
	document.getElementById("playerSP").style.width = (stamina / maxStamina) * 100 + '%';
}

function charPageUpdate() {
	document.getElementById('stats').innerHTML = '<li class="list-group-item">Nivel: ' + level + ' (' + experience + '/' + reqExp[level] + ')</li><li class="list-group-item">Vida: ' + health + '/' + maxHealth + '</li><li class="list-group-item">Vigor: ' + stamina + '/' + maxStamina + '</li><li class="list-group-item">Dano: ' + damage + '</li>';
	document.getElementById('equipments').innerHTML = '<li class="list-group-item">Arma: ' + yourWeapon + '</li><li class="list-group-item">Armadura: ' + yourArmor + '</li>';
}

function doChat() {
	var chatContent = document.getElementById('chatSequence').value;
	if (chatContent == "grains") {
		grainR += 2500;
		resourceCheck();
	} else if (chatContent == "nqueroclicaremnada") {
		b1Clicks = 30;
		b2Clicks = 30;
		b3Clicks = 30;
		countRat = 10;
		countSaqueador = 10;
		countSaqueadorPest = 1;
		ironR = 5;
		canMine = 1;
		canFish = 1;
		$('<button class="cooldown2">Minerar Ferro</button>').hide().appendTo("#buttonsCol").fadeIn(1000);
		$('<button class="cooldown3">Pescar</button>').hide().appendTo("#buttonsCol").fadeIn(1000);
		charPageUpdate();
		resourceCheck();
	} else if (chatContent == "pelospoderesdegrayskull") {
		damage += 1000;
		charPageUpdate();
		alert("Você agora é o He-Man");
	}
	else {
		chatMessage('Logical error.');
	}
	document.getElementById('chatSequence').value = "";
}

function setMonsterInfo(arg1, arg2, arg3, arg4) {
	enemyName = arg1;
	enemyHealth = arg2;
	enemyMaxHealth = enemyHealth;
	enemyStamina = arg3;
	enemyMaxStamina = enemyStamina;
	enemyDamage = arg4;
}

function levelUp() {
	level++;
	experience = 0;
	maxHealth += 100;
	health = maxHealth;
	stamina = maxStamina;
	damage += 5;
	charPageUpdate();
	chatMessage('Você se sente mais poderoso.');
}

function formatAMPM(date) {
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? hours : 12;
	minutes = minutes < 10 ? '0' + minutes : minutes;
	var strTime = hours + ':' + minutes + ' ' + ampm;
	return strTime;
}

function clearBattle() {
	$('#noStaminaAlert').hide();
	document.getElementById('yourName').innerHTML = '';
	document.getElementById('enemyName').innerHTML = '';
	document.getElementById('playerImage').innerHTML = '';
	$("#playerImageContainer").hide();
	document.getElementById('enemyImage').innerHTML = '';
	$("#enemyImageContainer").hide();
	document.getElementById('playerCombatHP').innerHTML = '';
	document.getElementById('playerCombatSP').innerHTML = '';
	document.getElementById('enemyCombatHP').innerHTML = '';
	document.getElementById('enemyCombatSP').innerHTML = '';
	document.getElementById('combatButtons').innerHTML = '';
}

function chatMessage(arg) {
	$($.parseHTML('<li class="list-group-item" style="border-radius:0 !important; border:none !important;"><b>[' + formatAMPM(new Date) + ']</b> <span class="font-italic">' + arg + '</span></li>')).hide().prependTo("#story").fadeIn(1000);
	tStories++;
	if (tStories > 5) {
		$('#story li:last').remove();
		tStories--;
	}
}

function successChatMessage(arg) {
	$('<li class="list-group-item bg-success" style="border-radius:0 !important; border:none !important;"><b>[' + formatAMPM(new Date) + ']</b> ' + arg + '</li>').hide().prependTo("#story").fadeIn(1000);
	tStories++;
	if (tStories > 5) {
		$('#story li:last').remove();
		tStories--;
	}
}

function dangerChatMessage(arg) {
	$('<li class="list-group-item bg-danger" style="border-radius:0 !important; border:none !important;"><b>[' + formatAMPM(new Date) + ']</b> ' + arg + '</li>').hide().prependTo("#story").fadeIn(1000);
	tStories++;
	if (tStories > 5) {
		$('#story li:last').remove();
		tStories--;
	}
}

function checkCheck2() {
	if (ironR == 5 && curLoc == locations[1] && countSaqueadorPest == 0) {
		$('#battle').modal('hide');
		tCLogs = 0;
		document.getElementById('combatResults').innerHTML = '';
		document.getElementById('combatLog').innerHTML = '';
		charPageUpdate();
		chatMessage('Com um golpe final, a <i>coisa</i> cai e não se move mais. Você não pode acreditar que tais horrores possam existir. Você deve voltar ao campo e dar ao Guardião os minerios de ferro');
		$(':button').prop('disabled', true);
		setTimeout(function () {
			$(':button').prop('disabled', false);
			cutscene1();
		}, 6000);
	}
}

function checkCheck4() {
	if (countMini == 4) {
		document.getElementById('combatResults').innerHTML = '';
		document.getElementById('combatLog').innerHTML = '';
		$('#battle').modal('hide');
		$(':button').prop('disabled', true);
		setTimeout(function () {
			$(':button').prop('disabled', false);
			cutscene3();
		}, 6000);
	}
}

function resourceCheck() {
	document.getElementById('resources').innerHTML = "";
	if (grainR > 0) {
		document.getElementById('resources').innerHTML += "<li id='grainR' class='list-group-item'>Grãos: <span id='grainID'>" + grainR + "</span></li>";
	}
	if (ironR > 0) {
		document.getElementById('resources').innerHTML += "<li id='ironR' class='list-group-item'>Minerios de Ferro: <span id='ironID'>" + ironR + "</span></li>";
	}
	if (fishR > 0) {
		document.getElementById('resources').innerHTML += "<li id='fishR' class='list-group-item'>Peixes: <span id='fishID'>" + fishR + "</span></li>";
	}
}

function locationCheck() {
	document.getElementById('placeName').innerHTML = "";
	document.getElementById('placeDesc').innerHTML = "";
	document.getElementById('placeActions').innerHTML = "";
	$($.parseHTML(curLoc)).appendTo("#placeName");
	$($.parseHTML(curDesc)).appendTo("#placeDesc");
	$($.parseHTML(curActions)).appendTo("#placeActions");
}

function gotoLocation(arg) {
	if (arg === 1 && canMine == 0) {
		$('#lockedAlert').show();
		return;
	} else if (arg == 1 && countRat < 10) {
		$('#lockedAlert').show();
		return;
	}
	if (arg === 2 && ironR < 5) {
		$('#lockedAlert').show();
		return;
	} else if (arg === 2 && countSaqueador < 10) {
		$('#lockedAlert').show();
		return;
	}
	if (arg === 3 && canFish == 0 && canMine == 0) {
		$('#lockedAlert').show();
		return;
	}
	else {
		$('#lockedAlert').hide();
		curLoc = locations[arg];
		curDesc = locDesc[arg];
		curActions = locActions[arg];
		locationCheck();
		chatMessage('Você chegou em ' + curLoc.toLowerCase() + '.');
		$('#worldMap').modal('hide');
	}
	// else if (arg == 2 && canFish == 0) {
	// 	$('#lockedAlert').show();
	// 	return;
	// }
}

function giveEXP() {
	if (curLoc == locations[0]) {
		experience += 25;
		if (experience >= reqExp[level]) {
			levelUp();
		}
	} else if (curLoc == locations[1]) {
		experience += 45;
		if (experience >= reqExp[level]) {
			levelUp();
		}
	} else if (curLoc == locations[2]) {
		experience += 100;
		if (experience >= reqExp[level]) {
			levelUp();
		}
	}
}

function cutscene1() {
	countSaqueadorPest++;
	$('#main').fadeTo(500, 0, function () {
		document.getElementById('main').style.display = "none";
		$('#cutscenes').fadeTo(500, 1, function () {
			document.getElementById('cutscenes').style.display = "";
			setTimeout(function () {
				$('#cutsceneText').fadeTo(500, 1, function () {
					$($.parseHTML('<span class="font-weight-bold" style="font-size: 2em">17 Anos atrás</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
				});
				setTimeout(function () {
					$('#cutsceneText').fadeTo(500, 0, function () {
						document.getElementById('cutsceneText').innerHTML = "";
					});
					setTimeout(function () {
						$('#cutsceneText').fadeTo(500, 1, function () {
							$($.parseHTML('<span class="font-italic" style="font-size: 1.5em">Era um dia comum, como qualquer outro</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
						});
						setTimeout(function () {
							$('#cutsceneText').fadeTo(500, 0, function () {
								document.getElementById('cutsceneText').innerHTML = "";
							});
							setTimeout(function () {
								$('#cutsceneText').fadeTo(500, 1, function () {
									$($.parseHTML('<span class="font-italic" style="font-size: 1.5em">Como qualquer outro dia todos sentavam em suas mesas para assistir o jornal apos um dia de trabalho.</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
								});
								setTimeout(function () {
									$('#cutsceneText').fadeTo(500, 0, function () {
										document.getElementById('cutsceneText').innerHTML = "";
									});
									setTimeout(function () {
										$('#cutsceneText').fadeTo(500, 1, function () {
											$($.parseHTML('<span class="font-italic" style="font-size: 1.5em">Mas aquele dia não foi como os outros.</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
										});
										setTimeout(function () {
											$('#cutsceneText').fadeTo(1000, 0, function () {
												document.getElementById('cutsceneText').innerHTML = "";
												document.getElementById('cutscenes').style.display = "none";
												$('#main').fadeTo(3000, 1, function () {
													document.getElementById('main').style.display = "";
												});
											});
										}, 7500);
									}, 1000);
								}, 7500);
							}, 1000);
						}, 7500);
					}, 1000);
				}, 3500);
			}, 1000);
		});
	});
	// silentSave();
}

function cutscene2() {
	$('#main').fadeTo(500, 0, function () {
		document.getElementById('main').style.display = "none";
		$('#cutscenes').fadeTo(500, 1, function () {
			document.getElementById('cutscenes').style.display = "";
			setTimeout(function () {
				$('#cutsceneText').fadeTo(500, 1, function () {
					$($.parseHTML('<span class="font-italic" style="font-size: 1.5em">Após a chuva nuclear chegar eles vinheram e mataram meu cachorro.</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
				});
				setTimeout(function () {
					$('#cutsceneText').fadeTo(500, 0, function () {
						document.getElementById('cutsceneText').innerHTML = "";
					});
					setTimeout(function () {
						$('#cutsceneText').fadeTo(500, 1, function () {
							$($.parseHTML('<span class="font-italic" style="font-size: 1.5em">Eles levaram meu amado fiilho tambem.</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
						});
						setTimeout(function () {
							$('#cutsceneText').fadeTo(500, 0, function () {
								document.getElementById('cutsceneText').innerHTML = "";
							});
							setTimeout(function () {
								$('#cutsceneText').fadeTo(500, 1, function () {
									$($.parseHTML('<span class="font-italic" style="font-size: 1.5em">Minhas memórias são tudo que me resta.</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
								});
								setTimeout(function () {
									$('#cutsceneText').fadeTo(500, 0, function () {
										document.getElementById('cutsceneText').innerHTML = "";
									});
									setTimeout(function () {
										$('#cutsceneText').fadeTo(500, 1, function () {
											$($.parseHTML('<span class="font-italic" style="font-size: 1.5em">Acho que logo eles vão vir me levar tbm.</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
										});
										setTimeout(function () {
											$('#cutsceneText').fadeTo(1000, 0, function () {
												document.getElementById('cutsceneText').innerHTML = "";
												document.getElementById('cutscenes').style.display = "none";
												$('#main').fadeTo(2000, 1, function () {
													document.getElementById('main').style.display = "";
													$('#battle').modal({
														backdrop: 'static',
														keyboard: false
													});
													$('#battle').modal('toggle');
													initializeCombat();
												});
											});
										}, 7500);
									}, 1000);
								}, 7500);
							}, 1000);
						}, 7500);
					}, 1000);
				}, 7500);
			}, 1000);
		});
	});
}

function cutscene3() {
	$('#main').fadeTo(500, 0, function () {
		document.getElementById('main').style.display = "none";
		$('#cutscenes').fadeTo(500, 1, function () {
			document.getElementById('cutscenes').style.display = "";
			setTimeout(function () {
				$('#cutsceneText').fadeTo(500, 1, function () {
					$($.parseHTML('<span class="font-italic" style="font-size: 1.5em">Depois de você dar o golpe final no ultimo guardião, você se sente tonto.</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
				});
				setTimeout(function () {
					$('#cutsceneText').fadeTo(500, 0, function () {
						document.getElementById('cutsceneText').innerHTML = "";
					});
					setTimeout(function () {
						$('#cutsceneText').fadeTo(500, 1, function () {
							$($.parseHTML('<span class="font-italic" style="font-size: 1.5em">Quando a tontura passa você olha para suas mãos ensanguentadas.</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
						});
						setTimeout(function () {
							$('#cutsceneText').fadeTo(500, 0, function () {
								document.getElementById('cutsceneText').innerHTML = "";
							});
							setTimeout(function () {
								$('#cutsceneText').fadeTo(500, 1, function () {
									$($.parseHTML('<span class="font-italic font-weight-bold" style="font-size: 1.5em">Você pensa... O que foi que eu fiz?</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
								});
								setTimeout(function () {
									$('#cutsceneText').fadeTo(500, 0, function () {
										document.getElementById('cutsceneText').innerHTML = "";
									});
									setTimeout(function () {
										$('#cutsceneText').fadeTo(500, 1, function () {
											$($.parseHTML('<span class="font-italic" style="font-size: 1.5em"><img src="img/newspaper.png" style="max-height:100vh !important;" /></span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
										});
										setTimeout(function () {
											$('#cutsceneText').fadeTo(500, 0, function () {
												document.getElementById('cutsceneText').innerHTML = "";
											});
											setTimeout(function () {
												$('#cutsceneText').fadeTo(500, 1, function () {
													$($.parseHTML('<span class="font-italic" style="font-size: 1.5em">Fim</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
												});
												setTimeout(function () {
													$('#cutsceneText').fadeTo(1000, 0, function () {
														document.getElementById('cutsceneText').innerHTML = "";
														document.getElementById('cutscenes').style.display = "none";
														$('#startScreen').fadeTo(2000, 1, function () {
															document.getElementById('startScreen').style.display = "";
															location.reload();
														});
													});
												}, 5000);
											}, 1000);
										}, 7500);
									}, 1000);
								}, 7500);
							}, 1000);
						}, 7500);
					}, 1000);
				}, 7500);
			}, 1000);
		});
	});
}

function creditos() {
	$('#main').fadeTo(500, 0, function () {
		document.getElementById('main').style.display = "none";
		$('#cutscenes').fadeTo(500, 1, function () {
			document.getElementById('cutscenes').style.display = "";
			setTimeout(function () {
				$('#cutsceneText').fadeTo(500, 1, function () {
					$($.parseHTML('<span class="font-italic" style="font-size: 1.5em">Feito por Luiz Henrique</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
				});
				setTimeout(function () {
					$('#cutsceneText').fadeTo(500, 0, function () {
						document.getElementById('cutsceneText').innerHTML = "";
					});
					setTimeout(function () {
						$('#cutsceneText').fadeTo(500, 1, function () {
							$($.parseHTML('<span class="font-italic" style="font-size: 1.5em">Para a disciplina Desenvolvimentos de Jogos para Web</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
						});
						setTimeout(function () {
							$('#cutsceneText').fadeTo(500, 0, function () {
								document.getElementById('cutsceneText').innerHTML = "";
							});
							setTimeout(function () {
								$('#cutsceneText').fadeTo(500, 1, function () {
									$($.parseHTML('<span class="font-italic font-weight-bold" style="font-size: 1.5em">Todas imagens retiradas do site pixabay.com</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
								});
								setTimeout(function () {
									$('#cutsceneText').fadeTo(500, 0, function () {
										document.getElementById('cutsceneText').innerHTML = "";
									});
									setTimeout(function () {
										$('#cutsceneText').fadeTo(500, 1, function () {
											$($.parseHTML('<span class="font-italic" style="font-size: 1.5em"><img src="img/dead.jpg" style="max-height:70vh !important;" /></span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
										});
										setTimeout(function () {
											$('#cutsceneText').fadeTo(500, 0, function () {
												document.getElementById('cutsceneText').innerHTML = "";
											});
											setTimeout(function () {
												$('#cutsceneText').fadeTo(500, 1, function () {
													$($.parseHTML('<span class="font-italic" style="font-size: 1.5em">Fim</span>')).hide().prependTo("#cutsceneText").fadeIn(1000);
												});
												setTimeout(function () {
													$('#cutsceneText').fadeTo(1000, 0, function () {
														document.getElementById('cutsceneText').innerHTML = "";
														document.getElementById('cutscenes').style.display = "none";
														$('#startScreen').fadeTo(2000, 1, function () {
															document.getElementById('startScreen').style.display = "";
															location.reload();
														});
													});
												}, 5000);
											}, 1000);
										}, 7500);
									}, 1000);
								}, 7500);
							}, 1000);
						}, 7500);
					}, 1000);
				}, 7500);
			}, 1000);
		});
	});
}