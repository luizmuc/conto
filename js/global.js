// Character vars
var level = 1;
var experience = 0;
var reqExp = [0, 100, 200, 300, 500, 700, 1000, 1500, 2000, 3000, 999999999];
var health = 100;
var maxHealth = 100;
var stamina = 100;
var maxStamina = 100;
var damage = 10;
var weapons = ['Desarmado', 'Taco com Pregos', 'Facão'];
var yourWeapon = weapons[0];
var armors = ['Roupas Rasgadas', 'Colete Acolchoado', 'Armadura de Guardião'];
var yourArmor = armors[0];
// Enemy vars
var enemyName = "";
var enemyHealth = 0;
var enemyMaxHealth = 0;
var enemyStamina = 0;
var enemyMaxStamina = 0;
var enemyDamage = 0;
// Total clicks to progress story
var b1Clicks = 0;
var b2Clicks = 0;
var b3Clicks = 0;
// Resources
var grainR = 0;
var ironR = 0;
var fishR = 0;
var yourGold = 0;
// Booleans
var canMine = 0;
var canFish = 0;
var doBattle;
var dropGrain = 0;
var dropPerk = 0;
var grainPerk = false;
var extraDamage = false;
var selfHeal = false;
var findSearch = false;
// Locations and current
var locations = ['Campos Alagados', 'Minas Abandonadas', 'O Lago', 'Abrigo Nuclear'];
var locDesc = ['Após o período da grande chuva, os militares sobreviventes estabeleceram um novo governo. A maioria dos civis foram alocados como trabalhadores de plantio nos campos que restaram.', 'Esta mina foi abandonada durante as grandes chuvas. Você já ouviu diversas histórias de trabalhadores que se tornaram saqueadores aqui.', 'O lago é determinadamente proibido. Logo além dele está o abrigo nuclear. É lá onde os militares escolheram como base do novo governo. Ninguém sabe o que realmente acontece por lá.','O abrigo nuclear está muito alem da zona permitida. Se você for pego aqui vai ser o fim da linha.'];
var locActions = ['<button id="battleB" class="blankButton">Lutar</button> | <button id="restB" class="blankButton">Dormir</button> | <button id="searchB" class="blankButton">Procurar</button>', '<button id="battleB" class="blankButton">Lutar</button>', '<button id="battleB" class="blankButton">Lutar</button>', '<button id="battleB" class="blankButton">Lutar</button>'];
var curLoc = locations[0];
var curDesc = locDesc[0];
var curActions = locActions[0];
var countMini = 0;
var countRat = 0;
var countSaqueador = 0;
var countSaqueadorPest = 0;
// Total amount of messages in stories
var tStories = 0;
var tCLogs = 0;
